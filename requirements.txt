fastapi
uvicorn[standard]
json5
cysystemd==1.6.0
asyncinotify==4.0.9
requests==2.31.0