#!/usr/bin/env python3.8

from multiprocessing import Process
from pydantic import BaseModel
import json
from typing import List,Union
import json5
import uvicorn
from fastapi import FastAPI
from pathlib import Path
from shutil import rmtree
from asyncinotify import Inotify, Mask
import asyncio
import requests

import logging
from cysystemd import journal

appName="rundirCleaner"
port=5000
monitored_rundirs_save_file = "monitored_rundirs.json"

logging.basicConfig(handlers=[journal.JournaldLogHandler()], format='%(levelname)s: %(name)s: %(message)s', level=logging.DEBUG)
logging.getLogger('asyncio').setLevel(logging.WARNING)
logger = logging.getLogger(appName)

app = FastAPI()

class MonitoredRundir(BaseModel):
    runNumber: int
    rubus: List[str]

@app.get("/{runNumber}")
@app.get("/v1/run/{runNumber}")
def check_exist(runNumber: int):
    pathToCheck = get_rundir_path() + str(runNumber)
    if (Path(pathToCheck).exists()):
        logger.info(f"Directory {pathToCheck} exists.")
        return {"exists": True, "path": pathToCheck}
    else:
        logger.info(f"Directory {pathToCheck} doesn't exist.")
        return {"exists": False, "path": pathToCheck}

@app.delete("/{runNumber}")
@app.delete("/v1/run/{runNumber}")
def delete(runNumber: int, dummyMode: Union[bool, None] = None):
    pathToDelete = get_rundir_path() + str(runNumber)
    if not dummyMode:
        logger.info(f"Deleting directory {pathToDelete}")
        rmtree(pathToDelete, True)
    else:
        logger.info(f"Would have deleted directory {pathToDelete}, but called in dummy mode, so doing nothing.")
    return {"path": pathToDelete}

def deleteRundirsOnRUBUs(monitoredRundir : MonitoredRundir):
    for rubu in monitoredRundir.rubus:
        try:
            requests.delete(f'http://{rubu}:{port}/v1/run/{monitoredRundir.runNumber}')
            deleteRundirFromSavefile(monitoredRundir.runNumber)
        except:
            logger.error(f"Couldn't contact {rubu} to delete rundir for run {monitoredRundir.runNumber}.")

@app.post("/v1/monitor")
async def monitor(monitoredRundir : MonitoredRundir):
    addRundirToSavefile(monitoredRundir)
    asyncio.create_task(monitor_dir(monitoredRundir))
    return {"path": constructRundirFromRunnumber(monitoredRundir.runNumber)}

def constructRundirFromRunnumber(runNumber : int):
    return get_rundir_path() + str(runNumber)

def get_rundir_path():
    config_file = "/etc/scdaq/scdaq.json5"
    pathPrefix = "/fff/ramdisk" + '/run'
    try:
        with open(config_file, 'r') as f:
            config = json5.load(f)
            pathPrefix = config['output_filename_base'] + '/run'
    except (FileNotFoundError, PermissionError, OSError):
        logger.warning(f"Couldn't open {config_file} to retrieve run directory. Using default of {pathPrefix}.")
    except KeyError:
        logger.warning(f"Couldn't find key 'output_filename_base' in {config_file} to retrieve run directory. Using default of {pathPrefix}.")
    return pathPrefix

async def monitorSavedRundirs(monitored_rundirs : dict):
    await asyncio.gather(*[monitor_dir(MonitoredRundir.model_validate_json(m)) for m in monitored_rundirs.values()])

async def monitor_dir(monitoredRundir : MonitoredRundir):
    logger.info(f"Will monitor {constructRundirFromRunnumber(monitoredRundir.runNumber)} and trigger deletion on secondary RUBUs once it has been removed.")
    with Inotify() as inotify:
        try:
            # If the monitored directory is deleted we break out of the for loop
            inotify.add_watch(constructRundirFromRunnumber(monitoredRundir.runNumber), Mask.DELETE_SELF)
            async for event in inotify:
                logger.info(f"{event.path} was deleted. Issuing delete command to other RUBUs.")
                break
        except FileNotFoundError:
            logger.info(f"{constructRundirFromRunnumber(monitoredRundir.runNumber)} doesn't appear to exist (anymore). Issuing delete command to other RUBUs.")
        deleteRundirsOnRUBUs(monitoredRundir)

def addRundirToSavefile(monitoredRundir : MonitoredRundir):
    monitored_rundirs = loadSavedRundirs()
    monitored_rundirs[str(monitoredRundir.runNumber)] = monitoredRundir.model_dump_json()
    writeMonitoredRundirSavefile(monitored_rundirs)

def deleteRundirFromSavefile(run : int):
    monitored_rundirs = loadSavedRundirs()
    del monitored_rundirs[str(run)]
    writeMonitoredRundirSavefile(monitored_rundirs)

def writeMonitoredRundirSavefile(monitored_rundirs : dict):
    with open(monitored_rundirs_save_file, "w") as f:
        json.dump(monitored_rundirs, f)

def loadSavedRundirs():
    try:
        with open(monitored_rundirs_save_file, 'r') as f:
            return json.load(f)
    except FileNotFoundError:
        logger.info("No save file found. Creating a new one.")
    except json.JSONDecodeError:
        logger.warning(f"Existing save file {monitored_rundirs_save_file} couldn't be parsed. Will be discarded.")
    return {}

def loadAndMonitorSavedRundirs():
    monitored_rundirs = loadSavedRundirs()
    asyncio.run(monitorSavedRundirs(monitored_rundirs))

def main():
    p = Process(target=loadAndMonitorSavedRundirs)
    p.start()

    uvicorn.run(appName + ":app", host="0.0.0.0", port=port, log_level="warning")

if __name__ == '__main__':
    main()
