#!/bin/bash

DIR="$(pwd)"
cd $DIR

rm -rf env

export PATH=$PATH:/usr/local/bin
python3.8 -m venv env

source env/bin/activate

python3.8 -m pip install --upgrade pip
python3.8 -m pip install --upgrade setuptools
python3.8 -m pip install -r requirements.txt
