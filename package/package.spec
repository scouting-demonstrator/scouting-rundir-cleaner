%define _build_id_links none
%define _prefix  /opt/l1scouting-rundircleaner
%define _systemd /etc/systemd/system
%define _homedir %{_prefix}/rundircleaner
%define debug_package %{nil}

# To skip bytecompilation
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')

Name: scouting-rundir-cleaner
Version: %{_version}
Release: %{_release}
Summary: CMS L1 Scouting API to remove rundirectories for completed runs
Group: CMS/L1Scouting
License: GPL
Vendor: CMS/L1Scouting
Packager: %{_packager}
Source: %{name}.tar
ExclusiveOs: linux
Provides: scouting-rundir-cleaner
Requires: python38,scdaq,systemd-devel

Prefix: %{_prefix}

%description
CMS L1 Scouting API to remove rundirectories for completed runs

%files
%defattr(-,scouter,root,-)
%attr(-, scouter, root) %dir %{_prefix}
%attr(-, scouter, root) %dir %{_homedir}
%attr(-, scouter, root) %{_homedir}/*
%attr(644, root, root) %config %{_systemd}/rundirCleaner.service

%prep
%setup -c

%build

%install
[ $RPM_BUILD_ROOT != / ] && rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_prefix}
pwd
tar cf - . | (cd  $RPM_BUILD_ROOT%{_prefix}; tar xfp - )
mkdir -p $RPM_BUILD_ROOT%{_systemd}
mv $RPM_BUILD_ROOT%{_homedir}/systemd/rundirCleaner.service $RPM_BUILD_ROOT%{_systemd}

%clean
[ $RPM_BUILD_ROOT != / ] && rm -rf $RPM_BUILD_ROOT || :

%pre

# Upgrading
if [ $1 -gt 1 ] ; then
  systemctl stop rundirCleaner
  systemctl daemon-reload
fi

%post

# Register the service and start it
systemctl daemon-reload
systemctl enable --now rundirCleaner

%preun

# Stopping service before removal
if [ $1 -eq 0 ] ; then
  systemctl stop rundirCleaner
fi

%postun

# Only for uninstall!
if [ $1 -eq 0 ] ; then

  # Removing folder
  systemctl daemon-reload
  rm -fR %{_homedir}
  rmdir --ignore-fail-on-non-empty %{_prefix}

fi
